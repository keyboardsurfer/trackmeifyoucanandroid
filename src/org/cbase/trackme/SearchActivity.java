package org.cbase.trackme;

import java.util.Date;
import java.util.List;

import org.cbase.trackme.positioning.QueryRequest;
import org.cbase.trackme.positioning.SaveRequest;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.inject.Inject;

public class SearchActivity extends RoboActivity {

   private static String TAG = SearchActivity.class.getSimpleName();
   // private String[] locations = {};

   @Inject
   WifiManager wifiManager;
   @InjectView(R.id.searchSwitchBtn)
   Button searchButton;
   @InjectView(R.id.searchET)
   EditText searchEditText;

   private Handler handler;
   private List<ScanResult> scanResults;

   private WifiReceiver receiverWifi;

   private boolean save = false;
   private boolean update = false;

   Dialog waitDialog = null; 
   
   @Override
   public void onCreate(Bundle savedInstanceState) {

      super.onCreate(savedInstanceState);
      setContentView(R.layout.search_layout);

      handler = new Handler();

      wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

      WifiInfo wi = wifiManager.getConnectionInfo();
      String ma = wi.getMacAddress();

      Log.d(TAG, "ma: " + ma);

      List<ScanResult> scanResults = wifiManager.getScanResults();
      if (scanResults != null) {
         System.out.println(scanResults.size() + " wifi endpoints found:");
         for (ScanResult scanResult : scanResults) {
            Log.d(TAG, "- " + scanResult.BSSID + " " + scanResult.toString());
         }
      }

      receiverWifi = new WifiReceiver();

      searchButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            Toast.makeText(SearchActivity.this, "Save ...", Toast.LENGTH_SHORT)
                  .show();

            save = true;
            wifiManager.startScan();

         }
      });

   }

   @Override
   protected void onPause() {
      unregisterReceiver(receiverWifi);
      super.onPause();
   }

   @Override
   protected void onResume() {
      registerReceiver(receiverWifi, new IntentFilter(
            WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

      Log.d(TAG, "startScan");
      wifiManager.startScan();
      super.onResume();
   }

   private void updateMyLocation() {
      new QueryRequest(scanResults, this) {

         @Override
         protected void finish(final String location) {
            Log.d(TAG, "=========================== HAT GEKLAPPT!!!: " + this);
            Log.d(TAG, "===========================        location: "
                  + location);

            handler.post(new Runnable() {
               @Override
               public void run() {
                  
               }
            });

         }
      }.start();

   }

   class WifiReceiver extends BroadcastReceiver {
      private String TAG = "WifiReceiver";

      @Override
      public void onReceive(Context c, Intent intent) {
         Log.i(TAG, "Received wifi scans");

         scanResults = wifiManager.getScanResults();
         for (ScanResult scanResult : scanResults) {
            Log.d(TAG, "- " + scanResult.BSSID + " SSID: " + scanResult.SSID
                  + " " + scanResult.level);
         }
         String room = searchEditText.getText().toString();
         if (TextUtils.isEmpty(room)) {
            room = "the awesome room";
         }
         if (save) {

            new SaveRequest(new Date(), scanResults, room, SearchActivity.this) {
               @Override
               protected void finish() {
                  Log.d(TAG, "=========================== HAT GEKLAPPT!!!: "
                        + this);

               }

            }.start();

            // new AnnounceSessionRequest("Dr. Jekyll") {
            // @Override
            // protected void finish() {
            // Log.d(TAG, "=========================== HAT GEKLAPPT!!!: " +
            // this);
            //
            // }
            // }.start();
         }
         save = false;

         if (update) {
            updateMyLocation();
         }
         update = false;

         StringBuilder sb = new StringBuilder();
         for (ScanResult result : scanResults) {
            sb.append(result.BSSID).append(" ").append(result.level)
                  .append("\n");
         }

         // mainText.setText(sb);

         // wifiManager.startScan();
      }
   }

}
