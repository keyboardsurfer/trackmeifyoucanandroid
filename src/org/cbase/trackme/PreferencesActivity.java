package org.cbase.trackme;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PreferencesActivity extends Activity implements OnClickListener {

   private static String TAG = PreferencesActivity.class.getSimpleName();

   private Button prefsSetBtn;
   private Button prefs1Btn;
   private Button prefs2Btn;
   private Button prefs3Btn;
   private Button prefs4Btn;
   private EditText prefs1ET;
   private EditText prefs2ET;
   private EditText prefs3ET;
   private EditText prefs4ET;

   private SharedPreferences sharedPrefs;

   public final static String KEY_URL_LOCATIONSRVR       = "URL_LOCSRVR";
   public final static String KEY_URL_ANNOUNCEMENTSRVR   = "URL_ANNSRVR";
   public final static String KEY_URL_USERNAME           = "USRNAME";
   public final static String KEY_URL_UPDATEINTERVAL     = "UPDINTERVAL";
   public final static String KEY_PREFS_INITIALIZED      = "INITZLD";
   public final static String KEY_PREFS_LASTPOSSRVRESULT = "LASTPOSSRVRES";
   

   private final static String USERNAMEFAIL = "user failed to set name";
   private static final String ANNSRVR = "http://84.22.107.94:4567/";
   // alternativer server
   //private static final String LOCSRVR = "http://f-n-o-r-d.appspot.com/location/";
   private static final String LOCSRVR = "http://trackme-ifyoucan.appspot.com/location/";
   private static final int UPDINTERVAL = 0;
   
   // -----------------------

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      if (savedInstanceState != null) {
         // handle sis
      }
      setContentView(R.layout.preferences_layout);

      prefsSetBtn = (Button) findViewById(R.id.prefsSetBtn);
      prefs1Btn = (Button) findViewById(R.id.prefs1Btn);
      prefs2Btn = (Button) findViewById(R.id.prefs2Btn);
      prefs3Btn = (Button) findViewById(R.id.prefs3Btn);
      prefs4Btn = (Button) findViewById(R.id.prefs4Btn);

      prefsSetBtn.setOnClickListener(this);
      prefs1Btn.setOnClickListener(this);
      prefs2Btn.setOnClickListener(this);
      prefs3Btn.setOnClickListener(this);
      prefs4Btn.setOnClickListener(this);

      prefs1ET = (EditText) findViewById(R.id.prefs1ET);
      prefs2ET = (EditText) findViewById(R.id.prefs2ET);
      prefs3ET = (EditText) findViewById(R.id.prefs3ET);
      prefs4ET = (EditText) findViewById(R.id.prefs4ET);

      sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

      try{
         prefs1ET.setText(sharedPrefs.getString(KEY_URL_USERNAME, USERNAMEFAIL));
         prefs2ET.setText(sharedPrefs.getString(KEY_URL_LOCATIONSRVR, LOCSRVR));
         prefs3ET.setText(sharedPrefs.getString(KEY_URL_ANNOUNCEMENTSRVR, ANNSRVR));
         prefs4ET.setText(""+sharedPrefs.getInt(KEY_URL_UPDATEINTERVAL, 0));
      } catch (Exception e) {
         Log.e(TAG, "something happened while reading shared prefs");
      }
   }


   @Override
   public void onBackPressed() {
      super.onBackPressed();
      if(sharedPrefs.getBoolean(KEY_PREFS_INITIALIZED, false) == true) {
         finish();
         return;
      }
      else {
         toast("set prefs first");
      }
   }

   @Override
   protected void onPause() {
      super.onPause();
   }

   @Override
   protected void onResume() {
      super.onResume();
   }

   @Override
   public void onClick(View v) {
      SharedPreferences.Editor editor = sharedPrefs.edit();

      switch(v.getId()) {

      case R.id.prefs1Btn:
         processUsername(editor);
         break;

      case R.id.prefs2Btn: 
         processLocSrvr(editor);
         break;

      case R.id.prefs3Btn: 
         processAnnSrvr(editor);
         break;

      case R.id.prefs4Btn: 
         processIntervall(editor);
         break;

      case R.id.prefsSetBtn: 

         boolean res = true;
         res &= processUsername(editor);
         res &= processLocSrvr(editor);
         res &= processAnnSrvr(editor);
         res &= processIntervall(editor);
         if(res==true) {
            editor.putBoolean(KEY_PREFS_INITIALIZED, true);
            editor.commit();
            
            setResult(RESULT_OK);
            finish();
         } else {
            toast("check values");
         }
         break;
      }
   }


   private boolean processIntervall(SharedPreferences.Editor editor) {
      Editable tmpE;
      String tmpS;
      tmpE = prefs4ET.getEditableText();
      if(tmpE!=null) {
         tmpS = tmpE.toString();
         if(tmpS!=null && tmpS.trim().length()>0) {
            try {
               int i = Integer.parseInt(tmpS);
               editor.putInt(KEY_URL_UPDATEINTERVAL, i);
               //toast("interval saved");
               return true;
            } catch(Exception e) {
               prefs4ET.setText("0");                                   
            }

         } else {
            prefs4ET.setText("0");
         }
      }
      else {
         prefs4ET.setText("0");
      }
      return false;
   }


   private boolean processAnnSrvr(SharedPreferences.Editor editor) {
      Editable tmpE;
      String tmpS;
      tmpE = prefs3ET.getEditableText();
      if(tmpE!=null) {
         tmpS = tmpE.toString();
         if(tmpS!=null && tmpS.trim().length()>0) {
            if(tmpS.startsWith("http://") || tmpS.startsWith("https://")) {
               editor.putString(KEY_URL_ANNOUNCEMENTSRVR, tmpS);
               //toast("announcement server saved");
               return true;
            }

         } else {
            prefs3ET.setText(ANNSRVR);
         }
      }
      else {
         prefs3ET.setText(ANNSRVR);
      }
      return false;
   }


   private boolean processLocSrvr(SharedPreferences.Editor editor) {
      Editable tmpE;
      String tmpS;
      tmpE = prefs2ET.getEditableText();
      if(tmpE!=null) {
         tmpS = tmpE.toString();
         if(tmpS!=null && tmpS.trim().length()>0) {
            if(tmpS.startsWith("http://") || tmpS.startsWith("https://")) {
               editor.putString(KEY_URL_LOCATIONSRVR, tmpS);
               //toast("location server saved");
               return true;
            }
         } else {
            prefs2ET.setText(LOCSRVR);
         }
      }
      else {
         prefs2ET.setText(LOCSRVR);
      }
      return false;
   }


   private boolean processUsername(SharedPreferences.Editor editor) {
      Editable tmpE;
      String tmpS;

      tmpE = prefs1ET.getEditableText();
      if(tmpE!=null) {
         tmpS = tmpE.toString();
         if(tmpS!=null && tmpS.trim().length()>0) {
            editor.putString(KEY_URL_USERNAME, tmpS);
            //toast("username saved");
            return true;
         } else {
            prefs1ET.setText(USERNAMEFAIL);
         }
      }
      else {
         prefs1ET.setText(USERNAMEFAIL);
      }
      return false;
   }


   void toast(String text) {
      Toast toast = Toast.makeText(this, text, Toast.LENGTH_LONG);
      toast.show();
   }


   // -------------------------------------------------------------------------

}
