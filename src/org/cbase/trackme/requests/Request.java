package org.cbase.trackme.requests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.preference.PreferenceManager;
import android.util.Log;

public class Request extends Thread {

   private static String TAG = Request.class.getSimpleName();
   /*
    * URL now provided by sharedprefs protected static final String
    * SERVICE_BASE_URI = "http://trackme-ifyoucan.appspot.com/location/";
    * protected static final String SERVICE_BASE_URI =
    * "http://f-n-o-r-d.appspot.com/location/"; protected static final String
    * SERVICE_BASE_URI = "http://molcounter01.appspot.com/location/";
    * 
    * protected static final String ANNOUNCE_BASE_URI =
    * "http://84.22.107.94:4567/";
    */

   protected static final Charset ENCODING = Charset.forName("UTF-8");
   private static final int SCRAPE_CONNECT_TIMEOUT = 5000;
   private static final int SCRAPE_READ_TIMEOUT = 15000;

   final public SharedPreferences prefs;

   public Request(Context context) {

      prefs = PreferenceManager.getDefaultSharedPreferences(context);
   }

   protected final void appendScanResults(
         final Uri.Builder uri,
         final List<ScanResult> scanResults) {
      for (ScanResult result : scanResults) {
         final String mac = result.BSSID.replaceAll(":", "");
         final String level = Integer.toString(result.level);
         uri.appendQueryParameter("mac" + mac, level);
      }
   }

   protected final URLConnection connect(final Uri uri) throws IOException,
         MalformedURLException {
      Log.d(TAG, "Uri: " + uri);
      final URLConnection connection = new URL(uri.toString()).openConnection();
      connection.setConnectTimeout(SCRAPE_CONNECT_TIMEOUT);
      connection.setReadTimeout(SCRAPE_READ_TIMEOUT);
      return connection;
   }

   protected void fail(final Exception x) {
      x.printStackTrace();
   }
}
