package org.cbase.trackme.util;

import java.util.ArrayList;

import org.cbase.trackme.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArlsAdapter extends ArrayAdapter<String> {

   private ArrayList<String> items;
   private Context context;

   public ArlsAdapter(Context context, int textViewResourceId, ArrayList<String> items) {
       super(context, textViewResourceId, items);
       this.context = context;
       this.items = items;
   }

   public View getView(int position, View convertView, ViewGroup parent) {
       View view = convertView;
       if (view == null) {
           LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           view = inflater.inflate(R.layout.list_item_position, null);
       }

       String item = items.get(position);
       if (item!= null) {
           // My layout has only one TextView
           TextView itemView = (TextView) view.findViewById(R.id.list_item_position_text);
           if (itemView != null) {
               // do whatever you want with your string and long
               itemView.setText(String.format("%s", item));
           }
        }
       return view;
   }
}