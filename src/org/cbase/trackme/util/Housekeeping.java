package org.cbase.trackme.util;

import android.app.Dialog;
import android.util.Log;

public class Housekeeping {


   private static String TAG = Housekeeping.class.getSimpleName();
   
   public static void secureDismissDialog(Dialog dialog) {
      try {
         if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
         }
      } catch (Exception e) {
         Log.w(TAG, "secureDismissDialog()");
      }
   }
}
