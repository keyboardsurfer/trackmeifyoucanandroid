package org.cbase.trackme.util;

import java.util.ArrayList;

import org.cbase.trackme.model.Position;
import org.cbase.trackme.model.ReducedScanResult;

public class RuntimeData {

   private static RuntimeData singleton = null;
   
   public static synchronized RuntimeData getInstance() {
      if (singleton == null) {
         singleton = new RuntimeData();
      }
      return singleton;
   }

   private ArrayList<ReducedScanResult> oldReducedScanResultARL = null;
   private ArrayList<Position> oldPositionARL = null;
   
   public ArrayList<ReducedScanResult> getOldReducedScanResultARL() {
      return oldReducedScanResultARL;
   }

   public void setOldReducedScanResultARL(ArrayList<ReducedScanResult> oldReducedScanResultARL) {
      this.oldReducedScanResultARL = oldReducedScanResultARL;
   }

   public ArrayList<Position> getOldPositionARL() {
      return oldPositionARL;
   }

   public void setOldPositionARL(ArrayList<Position> oldPositionARL) {
      this.oldPositionARL = oldPositionARL;
   }

}
