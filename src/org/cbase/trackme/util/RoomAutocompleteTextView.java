package org.cbase.trackme.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * User: biafra
 * Date: 2/19/12
 * Time: 2:14 PM
 */
public class RoomAutocompleteTextView extends AutoCompleteTextView {
  public RoomAutocompleteTextView(Context context) {
    super(context);
  }

  public RoomAutocompleteTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public RoomAutocompleteTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }
  protected CharSequence convertSelectionToString (Object selectedItem) {

    return (String) selectedItem;
  }


}
