package org.cbase.trackme.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import org.cbase.trackme.R;
import org.cbase.trackme.model.Room;

/**
 * User: biafra
 * Date: 2/18/12
 * Time: 4:49 PM
 */
public class RoomAdapter extends ArrayAdapter<Room> {

  private LayoutInflater inflater;
  private static final String TAG = RoomAdapter.class.getSimpleName();

  public RoomAdapter(Context context, int textViewResourceId, List<Room> objects) {
    super(context, textViewResourceId, objects);
    inflater = LayoutInflater.from(context);
    setNotifyOnChange(true);
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    if (convertView == null) {

      convertView = inflater.inflate(R.layout.list_item_room, parent, false);

      convertView.setTag(R.id.room_name, convertView.findViewById(R.id.room_name));
      convertView.setTag(R.id.user_names, convertView.findViewById(R.id.user_names));
    }
    TextView roomName = (TextView) convertView.getTag(R.id.room_name);
    Room room = getItem(position);
    Log.d(TAG, "room: " + room);
    roomName.setText(room.getRoom());

    TextView userNames = (TextView) convertView.getTag(R.id.user_names);
    StringBuilder stringBuilder = new StringBuilder();

    for(String name : room.getUsers()) {
      stringBuilder.append(name).append("\n");
    }
    userNames.setText(stringBuilder.toString());

    return convertView;
  }


}
