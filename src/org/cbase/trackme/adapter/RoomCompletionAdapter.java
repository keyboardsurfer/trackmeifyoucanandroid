package org.cbase.trackme.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import org.cbase.trackme.R;

//import com.jaeckel.amenoid.api.AmenService;
//import com.jaeckel.amenoid.api.model.Objekt;
//import com.jaeckel.amenoid.app.AmenoidApp;

/**
 * User: biafra
 * Date: 10/4/11
 * Time: 12:22 AM
 */
public class RoomCompletionAdapter extends ArrayAdapter<String> implements Filterable {

  private LayoutInflater inflater;
  private static final String TAG = "ObjektCompletionAdapter";
  private Typeface amenTypeBold;
  private Typeface amenTypeThin;


  public RoomCompletionAdapter(Context context, int textViewResourceId, List<String> objects) {
    super(context, textViewResourceId, objects);
    inflater = LayoutInflater.from(context);
  }

  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {

    View row = convertView;
    String objekt = getItem(position);
    if (row == null) {
      row = inflater.inflate(R.layout.list_item_room_name, parent, false);
      row.setTag(R.id.room_name, row.findViewById(R.id.room_name));

    }

    TextView textView = (TextView) row.findViewById(R.id.room_name);
    textView.setText(objekt);

    return row;
  }

  @Override
  public Filter getFilter() {

    return new Filter() {

      @Override
      public String convertResultToString(Object resultValue) {

        String o = (String) resultValue;

        return o;
      }


      @Override
      protected FilterResults performFiltering(CharSequence charSequence) {

        Log.d(TAG, "performFiltering: charSequence: " + charSequence);

        FilterResults results = new FilterResults();

        List<String> values = new ArrayList<String>();
        String[] array = new String[]{"Seminarraum", "HE1", "HE2", "Mainhall", "Bar",
                                      "Wercstatt", "Robolab", "Nerd Area", "c-lab", "Brücce",
                                      "HW-Lager", "cerverraum", "Weltenbaulab", "coundlab"};

        for (String s : array) {
          if (s.toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
            values.add(s);
          }
        }
        results.values = values;
        results.count = values.size();
        return results;
      }

      @Override
      protected void publishResults(CharSequence constraint,
                                    FilterResults results) {


        if (results != null && results.count > 0) {

          Log.d(TAG, "publishResults: Results changed. ");

          RoomCompletionAdapter.this.clear();
          List<String> list = (List<String>) results.values;
          for (String objekt : list) {
            RoomCompletionAdapter.this.add(objekt);
          }
          notifyDataSetChanged();
        }
      }
    };
  }
}



