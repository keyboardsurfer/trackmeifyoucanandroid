package org.cbase.trackme;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.cbase.trackme.adapter.RoomAdapter;
import org.cbase.trackme.announce.UserListRequest;
import org.cbase.trackme.model.Room;
import org.cbase.trackme.util.Gen;

/**
 * User: biafra
 * Date: 2/18/12
 * Time: 2:50 PM
 */
public class RoomListActivity extends ListActivity {

  private static String TAG = RoomListActivity.class.getSimpleName();
  private Handler     handler;
  private TextView    textView;
  private RoomAdapter adapter;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.room_list);

    handler = new Handler();

    textView = (TextView) findViewById(R.id.rooms);


  }

  public void onResume() {
    super.onResume();

    new UserListRequest(this) {
      @Override
      protected void finish(final String json) {

        Type listType = new TypeToken<List<Room>>() {
        }.getType();

        Gson gson = new Gson();
        List<Room> tmpRooms;
        try {
           tmpRooms = gson.fromJson(json, listType);
        } catch (Exception e) {
           Log.e(TAG, Gen.getStackTrace(e));
           tmpRooms = new ArrayList<Room>();
        }
        final List<Room> rooms = tmpRooms;
        
        Log.d("TAG", "------> response: " + json);
        Log.d("TAG", "------>    rooms: " + rooms);

        handler.post(new Runnable() {
          @Override
          public void run() {
            List<Room> notNullAndEmptyRooms = new ArrayList<Room>();
            for(Room room : rooms) {
              if (room.getRoom() != null && room.getUsers() != null && room.getUsers().size() > 0) {
                notNullAndEmptyRooms.add(room);
              }

            }
            adapter = new RoomAdapter(RoomListActivity.this,
                                      R.layout.list_item_room,
                                      notNullAndEmptyRooms);
            setListAdapter(adapter);

            Log.d(TAG, "Rooms: " + rooms.toString());
            textView.setText(json);

          }
        });
      }
    }.start();


  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.room_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.central_menu_prefs:
        Intent prefsIntent = new Intent(this, PreferencesActivity.class);
        this.startActivityForResult(prefsIntent, 0);
        break;

      case R.id.central_menu_search: {
        Intent searchIntent = new Intent(this, SearchActivity.class);
        this.startActivityForResult(searchIntent, 0);
        break;
      }

      case R.id.central_menu_central: {
        Intent roomList = new Intent(this, CentralActivity.class);
        this.startActivityForResult(roomList, 0);
        break;
      }

      case R.id.central_menu_debug: {
        Intent roomList = new Intent(this, DebugActivity.class);
        this.startActivityForResult(roomList, 0);
        break;
      }
    }
    return false;
  }
}