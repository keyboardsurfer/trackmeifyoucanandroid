package org.cbase.trackme;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class BgService extends Service {
   private static final String TAG = BgService.class.getSimpleName();

   private NotificationManager mNM;


   private int NOTIFICATION = R.string.bgservice_started;

   /**
    * Class for clients to access.  Because we know this service always
    * runs in the same process as its clients, we don't need to deal with
    * IPC.
    */
   public class LocalBinder extends Binder {
      BgService getService() {
         return BgService.this;
      }
   }

   @Override
   public void onCreate() {
      Log.d(TAG, "onCreate()");
      mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

      
      onCreateTimperpart();
      
      // Display a notification about us starting.  We put an icon in the status bar.
      showNotification();
   }

   @Override
   public int onStartCommand(Intent intent, int flags, int startId) {
      Log.d(TAG, "onStartCommand()");
      Log.i("LocalService", "Received start id " + startId + ": " + intent);
      // We want this service to continue running until it is explicitly
      // stopped, so return sticky.
      return START_STICKY;
   }

   @Override
   public void onDestroy() {
      Log.d(TAG, "onDestroy()");

      onDestroyTimerpart();
      // Cancel the persistent notification.
      mNM.cancel(NOTIFICATION);

      // Tell the user we stopped.
      Toast.makeText(this, R.string.bgservice_stopped, Toast.LENGTH_SHORT).show();
   }

   @Override
   public IBinder onBind(Intent intent) {
      Log.d(TAG, "onBind()");
      return mBinder;
   }

   // This is the object that receives interactions from clients.  See
   // RemoteService for a more complete example.
   private final IBinder mBinder = new LocalBinder();

   /**
    * Show a notification while this service is running.
    */
   private void showNotification() {
      Log.d(TAG, "showNotification()");

      // In this sample, we'll use the same text for the ticker and the expanded notification
      CharSequence text = getText(R.string.bgservice_started);

      // Set the icon, scrolling text and timestamp
      Notification notification = new Notification(R.drawable.logo, text,
            System.currentTimeMillis());

      // The PendingIntent to launch our activity if the user selects this notification
      PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
            new Intent(this, CentralActivity.class), 0);

      // Set the info for the views that show in the notification panel.
      notification.setLatestEventInfo(this, getText(R.string.bgservice_label),
            text, contentIntent);

      // Send the notification.
      mNM.notify(NOTIFICATION, notification);
   }
   
   
   // -----------------------------------------------------------------------
   private Timer timer;

   
   private TimerTask updateTask = new TimerTask() {
      @Override
      public void run() {
         Log.i(TAG, "Timer task doing work");
      }
   };
   
   void onCreateTimperpart() {
      
      timer = new Timer("Timer");
      timer.schedule(updateTask, 1000L, 60 * 1000L);
   }

   void onDestroyTimerpart() {

      timer.cancel();
      timer = null;
   }
}
