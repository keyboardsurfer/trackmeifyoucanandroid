package org.cbase.trackme.announce;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URLConnection;
import java.util.Date;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import org.cbase.trackme.PreferencesActivity;
import org.cbase.trackme.requests.Request;

/**
 * User: biafra
 * Date: 2/18/12
 * Time: 7:33 PM
 */
public class SaveRequest extends Request {

  private static String TAG = SaveRequest.class.getSimpleName();

  private Date   timestamp;
  private String location;
  private String session;

  public SaveRequest(String session, String location, Context context) {
    super(context);
    this.session = session;
    this.location = location;
  }

  public void run() {
    try {
      Uri.Builder uri = Uri.parse(prefs.getString(PreferencesActivity.KEY_URL_ANNOUNCEMENTSRVR, "no server") + "announce").buildUpon();

      final URLConnection connection = connect(uri.build());
      connection.setDoOutput(true);
      Log.d(TAG, "session=" + session);
      Log.d(TAG, "room=" + location);
      OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
      out.write("session=" + session + "&room=" + location);
      out.close();
      final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), ENCODING));

      String json = reader.readLine();
      reader.close();

      finish(json);

    } catch (final IOException x) {

      finish("failed to get location");

    } catch (final Exception x) {
      fail(x);
    }
  }

  protected void finish(final String location) {

  }
}
