package org.cbase.trackme.announce;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URLConnection;

import org.cbase.trackme.PreferencesActivity;
import org.cbase.trackme.requests.Request;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

/**
 * User: biafra
 * Date: 2/18/12
 * Time: 7:35 PM
 */
public class SessionRequest extends Request {
   
   private static String TAG = SessionRequest.class.getSimpleName();

   private String username;

   public SessionRequest(String username, Context context) {
      super(context);
      this.username = username;
   }

   public void run() {
      try {
         Uri.Builder uri = Uri.parse(prefs.getString(PreferencesActivity.KEY_URL_ANNOUNCEMENTSRVR, "no server") + "login").buildUpon();

         final URLConnection connection = connect(uri.build());
         connection.setDoOutput(true);

         OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
         out.write("user=" + username);
         out.close();


         final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), ENCODING));


         String json = reader.readLine();
         reader.close();

         finish(json);

      } catch (final IOException x) {
         Log.d(TAG, "failed to get session");
         finish("failed to get session");
         fail(x);
      } catch (final Exception x) {
         fail(x);
      }
   }

   protected void finish(final String location) {

   }
}
