package org.cbase.trackme.announce;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLConnection;

import android.content.Context;
import android.net.Uri;
import org.cbase.trackme.PreferencesActivity;
import org.cbase.trackme.requests.Request;

/**
 * User: biafra
 * Date: 2/19/12
 * Time: 2:21 PM
 */
public abstract class UserListRequest extends Request {

  public UserListRequest(Context context) {
    super(context);
  }

  public void run() {
    try {
      Uri.Builder uri = Uri.parse(prefs.getString(PreferencesActivity.KEY_URL_ANNOUNCEMENTSRVR, "no server") + "users/pretty").buildUpon();

      final URLConnection connection = connect(uri.build());
      final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), ENCODING));

      String json = reader.readLine();
      reader.close();

      finish(json);

    } catch (final IOException x) {

      finish("failed to get location");

    } catch (final Exception x) {
      fail(x);
    }
  }
  abstract protected void finish(final String location);
}
