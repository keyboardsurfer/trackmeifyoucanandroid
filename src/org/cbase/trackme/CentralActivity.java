package org.cbase.trackme;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.cbase.trackme.announce.SessionRequest;
import org.cbase.trackme.model.AnnounceSession;
import org.cbase.trackme.model.Fingerprint;
import org.cbase.trackme.model.Position;
import org.cbase.trackme.model.ReducedScanResult;
import org.cbase.trackme.positioning.QueryRequest;
import org.cbase.trackme.positioning.SaveRequest;
import org.cbase.trackme.util.RuntimeData;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CentralActivity extends Activity implements OnClickListener {

   Activity act = null;

   private static String TAG = CentralActivity.class.getSimpleName();
   public static final int REQUEST_CODE_LEARN = 0;
   public static final String ROOM_NAME = "room_name";

   Button positionLearnBtn;
   Button positionAckBtn;
   EditText positionET;
   Button positionUpdateBtn;
   ToggleButton positionUpdateSyncBtn;
   TextView positionDebugTV;

   // -------------------------

   private Handler handler;

   private WifiManager wifiManager;

   private WifiReceiver wifiReceiver;
   private List<ScanResult> scanResults = null;
   private long scanTimestamp = 0;

   private volatile boolean wantRoomSuggestion = false;

   private QueryRequest posQueryRequest = null;
   private SaveRequest posSaveRequest = null;

   private org.cbase.trackme.announce.SessionRequest annSessionRequest = null;
   private org.cbase.trackme.announce.SaveRequest annSaveRequest = null;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      Activity act = this;

      if (savedInstanceState != null) {
         // handle sis
      }
      setContentView(R.layout.central_layout);

      // startService(new Intent(this, BgService.class));

      handler = new Handler();

      positionLearnBtn = (Button) findViewById(R.id.positionLearnNewRoomBtn);
      positionAckBtn = (Button) findViewById(R.id.positionAckBtn);
      positionET = (EditText) findViewById(R.id.positionET);
      positionUpdateBtn = (Button) findViewById(R.id.positionUpdateBtn);
      positionUpdateSyncBtn = (ToggleButton) findViewById(R.id.positionAutosyncBtn);
      positionDebugTV = (TextView) findViewById(R.id.positionDebugTV);

      positionLearnBtn.setOnClickListener(this);
      positionAckBtn.setOnClickListener(this);
      positionUpdateBtn.setOnClickListener(this);
      positionUpdateSyncBtn.setOnClickListener(this);

      wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

      if (wifiManager != null && wifiManager.isWifiEnabled()) {
         WifiInfo wifiInfo = wifiManager.getConnectionInfo();
         if (wifiInfo != null) {
            String macAdr = wifiInfo.getMacAddress();
            Log.d(TAG, "macAdr :" + macAdr);

            scanResults = wifiManager.getScanResults();
            if (scanResults != null) {
               Log.d(TAG, "wifi endpoints found :" + scanResults.size());
               for (ScanResult scanResult : scanResults) {
                  Log.d(TAG, "wifi endpoint :" + scanResult.BSSID + " / "
                        + scanResult.toString());
               }
            } else {
               Log.d(TAG, "no wifi endpoints found.");
            }
         }
      } else {
         // ask user to enable wifi
      }

      wifiReceiver = new WifiReceiver();

      SharedPreferences sharedPrefs = PreferenceManager
            .getDefaultSharedPreferences(this);
      if (sharedPrefs.getBoolean(PreferencesActivity.KEY_PREFS_INITIALIZED,
            false) != true) {
         Intent prefsIntent = new Intent(this, PreferencesActivity.class);
         this.startActivity(prefsIntent);
      }
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.central_menu, menu);
      return true;
   }

   @Override
   protected void onPause() {
      super.onPause();

      if (wifiReceiver != null) {
         unregisterReceiver(wifiReceiver);
      } else {
         Log.d(TAG, "onPause() - wifiReceiver == null");
      }

   }

   @Override
   protected void onResume() {
      super.onResume();
      if (wifiReceiver != null) {
         registerReceiver(wifiReceiver, new IntentFilter(
               WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
         Log.d(TAG, "onResume() - wifiReceiver registriert");
      } else {
         Log.d(TAG, "onResume() - wifiReceiver == null");
      }

      if (wifiManager != null) {
         wifiManager.startScan();
         Log.d(TAG, "onResume() - wifiManager gestartet");
      } else {
         Log.d(TAG, "onResume() - wifiManager == null");
      }
   }

   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if (requestCode == REQUEST_CODE_LEARN && resultCode == RESULT_OK) {
         String roomName = data.getStringExtra(ROOM_NAME);
         positionET.setText(roomName);
      }
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
      case R.id.central_menu_prefs:
         Intent prefsIntent = new Intent(this, PreferencesActivity.class);
         this.startActivityForResult(prefsIntent, 0);
         break;

      case R.id.central_menu_search: {
         Intent searchIntent = new Intent(this, SearchActivity.class);
         this.startActivityForResult(searchIntent, 0);
         break;
      }

      case R.id.central_menu_room_list: {
         Intent roomList = new Intent(this, RoomListActivity.class);
         this.startActivityForResult(roomList, 0);
         break;
      }

      case R.id.central_menu_debug: {
         Intent roomList = new Intent(this, DebugActivity.class);
         this.startActivityForResult(roomList, 0);
         break;
      }
      }
      return false;
   }

   @Override
   public void onClick(View v) {

      switch (v.getId()) {

      case R.id.positionLearnNewRoomBtn: {
         Editable tmpRoomnameE = positionET.getEditableText();
         String tmpRoomname = "";
         if (tmpRoomnameE != null) {
            tmpRoomname = tmpRoomnameE.toString().trim();
         }
         Intent learnIntent = new Intent(this, LearnActivity.class);
         learnIntent.putExtra(LearnActivity.KEY_ROOMNAME, tmpRoomname);
         this.startActivityForResult(learnIntent, REQUEST_CODE_LEARN);
      }
      break;

      case R.id.positionAckBtn: {
         Editable tmpRoomnameE = positionET.getEditableText();
         String tmpRoomname = "";
         if (tmpRoomnameE != null) {
            tmpRoomname = tmpRoomnameE.toString().trim();
         }
         if (tmpRoomname.length() > 0) {
            final String room = tmpRoomname;

            // .............................................
            // contact position server
            if (scanResults != null && scanResults.size() > 0) {
               new org.cbase.trackme.positioning.SaveRequest(
                     new Date(),
                     scanResults, 
                     tmpRoomname, 
                     CentralActivity.this) {
                  @Override
                  protected void finish() {
                     Log.d(TAG, "SaveRequest.finish ");
                     handler.post(new Runnable() {
                        @Override
                        public void run() {
                           Toast.makeText(CentralActivity.this, "... saved position", Toast.LENGTH_SHORT).show();
                        }
                     });
                  }
               }.start();
            }

            // .............................................
            // contact announcement server

            // 1. get session here if we have none yet
            // TODO: use constant for username key here
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            new SessionRequest(
                  prefs.getString(PreferencesActivity.KEY_URL_USERNAME, "no username"),
                  CentralActivity.this) 
            {

               @Override
               protected void finish(final String location) {
                  Log.d(TAG, "AnnounceSessionRequest.result: " + location);

                  // 1. prepare save room to announcement server
                  if (!location.equals("failed to get location")) {

                     Gson gson = new Gson();
                     final AnnounceSession session = gson.fromJson(location, AnnounceSession.class);

                     handler.post( new Runnable() {
                        @Override
                        public void run() {

                           Toast.makeText(
                                 CentralActivity.this,
                                 "AnnounceSessionRequest: " + session.getSession(),
                                 Toast.LENGTH_SHORT).show();
                        }
                     });

                     // 2. save room to announcement server
                     new org.cbase.trackme.announce.SaveRequest(
                           session.getSession(), "room", CentralActivity.this) {

                        protected void finish(final String result) {

                           Log.d(TAG, "AnnounceSaveRequest.result: " + result);
                           handler.post(new Runnable() {
                              @Override
                              public void run() {

                                 Toast.makeText(CentralActivity.this,
                                       "AnnounceSaveRequest: " + result,
                                       Toast.LENGTH_SHORT).show();
                              }
                           });

                        }

                     }.start();
                  }
               }
            }.start();
         }
      }
      // .............................................
      break;

      case R.id.positionAutosyncBtn:
         // autosync on/off switch
         Toast.makeText(this, "tbd", Toast.LENGTH_SHORT);
         break;

      case R.id.positionUpdateBtn:
         if (wifiManager != null) {
            wifiManager.startScan();
            Log.d(TAG, "onResume() - wifiManager gestartet");
         } else {
            Log.d(TAG, "onResume() - wifiManager == null");
         }
         break;
      }
   }

   // ======================================================================

   void askForTheRightRoom(Context cont, ArrayList<Position> arl) {

      // final Dialog aDialog = new Dialog(cont);
      final AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
      final ListView roomLV = new ListView(this);
      final ArrayList<Position> poslist = arl;
      final ArrayAdapter posSingleListARAD = new ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1, 
            arl);

      roomLV.setAdapter(posSingleListARAD);

      dialogbuilder.setView(roomLV);
      dialogbuilder.setTitle("Select room");
      dialogbuilder.setNegativeButton("cancel",
            new android.content.DialogInterface.OnClickListener() {

         @Override
         public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
         }

      });
      final Dialog dialog = dialogbuilder.create();
      dialog.show();
      // positionET.setText("location");

      roomLV.setOnItemClickListener(new OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> parent, View view,
               int position, long id) {
            positionET.setText(poslist.get(position).getFingerprint()
                  .getRoom_name());
            dialog.dismiss();
         }
      });

   }

   // ---------------------------------------------------

   private class WifiReceiver extends BroadcastReceiver {

      private String TAG = WifiReceiver.class.getName();

      @Override
      public void onReceive(Context c, Intent intent) {
         Log.d(TAG, "onReceive() - received wifi scans");

         List<ScanResult> tmpScanResults = wifiManager.getScanResults();

         if (tmpScanResults != null && scanResults.size() > 0) {
            scanTimestamp = System.currentTimeMillis();
            scanResults = tmpScanResults;
            for (ScanResult scanResult : scanResults) {
               Log.d(TAG, "BSSID: " + scanResult.BSSID + " SSID: "
                     + scanResult.SSID + " Level: " + scanResult.level);
            }

            positionDebugTV.setText("got " + scanResults.size()
                  + " results at " + scanTimestamp);

            RuntimeData rtd = RuntimeData.getInstance();
            List<ReducedScanResult> oldScanResultARL = rtd.getOldReducedScanResultARL();
            List<ReducedScanResult> newScanResultARL = getScaledSortedPositionList_(tmpScanResults);

            if(isAskingTheUserNecessary_(oldScanResultARL, newScanResultARL)){
               posQueryRequest = new QueryRequest(scanResults, c) {

                  protected void finish(String posJsonStr) {
                     if (posJsonStr != null && posJsonStr.trim().length() > 0) {

                        Type listType = new TypeToken<List<Position>>() {}.getType();
                        Gson gson = new Gson();

                        ArrayList<Position> positionARL = null;
                        try {
                           positionARL = (ArrayList<Position>) gson.fromJson(posJsonStr, listType);
                        } catch (Exception e) {
                           positionARL = null;
                           posJsonStr = null;
                        }

                        if(positionARL != null && positionARL.size() > 0) {
                           askForTheRightRoom(act, positionARL);

                        } else {
                           Toast.makeText(CentralActivity.this, "think about learning this room", Toast.LENGTH_SHORT).show();
                        }
                        //                     if (positionARL != null && positionARL.size() > 0) {
                        //                        RuntimeData rtd = RuntimeData.getInstance();
                        //                        ArrayList<Position> oldPositionARL = rtd.getOldPositionARL();
                        //                        if(oldPositionARL != null && oldPositionARL.size() > 0) {
                        //                           if (isAskingTheUserNecessary(positionARL, oldPositionARL)) {
                        //                      askForTheRightRoom(act, positionARL);
                        //                              rtd.setOldPositionARL(oldPositionARL);
                        //                           }
                        //                        }
                        //                     }
                     }
                     //                  posQueryRequest = null;
                  }
               };
               posQueryRequest.run();
            }
         }

         // if (save) {
         //
         // new SaveRequest(new Date(), scanResults, room) {
         // @Override
         // protected void finish() {
         // Log.d(TAG, "=========================== HAT GEKLAPPT!!!: " + this);
         //
         // }
         //
         // }.start();
         // }
         // save = false;
         //
         // if (update) {
         // updateMyLocation();
         // }
         // update = false;
         //
         // StringBuilder sb = new StringBuilder();
         // for (ScanResult result : scanResults) {
         // sb.append(result.BSSID).append(" ").append(result.level).append("\n");
         // }
         // scanResultsView.setText(sb.toString());
         //
         // //mainText.setText(sb);
         //
         // // wifiManager.startScan();
      }

      protected boolean isAskingTheUserNecessary(ArrayList<Position> current,
            ArrayList<Position> previous) {
         current = getScaledSortedPositionList(current);
         previous = getScaledSortedPositionList(previous);
         int sizeLimit = Math.max(current.size(), previous.size());
         sizeLimit = Math.max(sizeLimit, 5);

         for (int i = 0; i < sizeLimit; i++) {
            Log.d(TAG,
                  "" + current.get(i).getConfidence() 
                  + " " + current.get(i).getFingerprint().getRoom_name() 
                  + " / " + previous.get(i).getConfidence() 
                  + " " + previous.get(i).getFingerprint().getRoom_name());
         }
         return true;
      }

      protected boolean isAskingTheUserNecessary_(List<ReducedScanResult> previous,
            List<ReducedScanResult> current) {
         if(previous == null || previous.size() == 0) {
            if(current == null || current.size() == 0) {
               return false;  // alles nix da
            } else {
               return true; // previous nix da, current aber doch
            }
         }
         if(current == null || current.size() == 0) {
            return false;
         }
           
         int limit = Math.max(current.size(), previous.size());
         limit = Math.min(limit, 5);

         Set<String> prev = new HashSet<String>();
         Set<String> cur = new HashSet<String>();
         Set<String> delta; // = new HashSet<String>();
               
         for (int i = 0; i < limit; i++) {
            Log.d(TAG,
                  "" + current.get(i).level
                  + " " + current.get(i).BSSID
                  + " / " + previous.get(i).level
                  + " " + previous.get(i).BSSID);
            prev.add(previous.get(i).BSSID);
            prev.add(previous.get(i).BSSID);
         }
         cur.removeAll(prev);
         if(cur.size()>=1)   {
            return true;
         }
         else { 
            return false;
         }
      }

      ArrayList<ReducedScanResult> getScaledSortedPositionList_(
            List<ScanResult> scanResultArl) {
         ArrayList<ReducedScanResult> result = null;
         if (scanResultArl == null) return null;
         
         TreeMap<Integer, String> map = new TreeMap<Integer, String>();
         int min = 0;
         for (int i = 0; i < scanResultArl.size(); i++) {
            ScanResult sr =  scanResultArl.get(i);
            Integer level = 0;
            try {
               level = sr.level;
               if (sr.BSSID != null) {
                  map.put(level, sr.BSSID);
                  min = Math.min(min, level);
               }
            } catch (Exception e) {
               Log.d(TAG, "skipping scsanresult", e);
            }
         }

         if (min >= 0)
            return null;

         result = new ArrayList<ReducedScanResult>();
         for (Map.Entry<Integer, String> entry : map.entrySet()) {
            int key = (int)(entry.getKey() * 100 / min);
            String value = entry.getValue();
            ReducedScanResult sr = new ReducedScanResult();
            sr.level = key;
            sr.BSSID = value;
            result.add(sr);
         }
         return result;
      }
   }



   ArrayList<Position> getScaledSortedPositionList(
         ArrayList<Position> positionArl) {
      ArrayList<Position> result = null;

      TreeMap<Float, String> map = new TreeMap<Float, String>();
      float max = 0;
      for (int i = 0; i < positionArl.size(); i++) {
         float confidence = 0;
         try {
            confidence = Float
                  .parseFloat(positionArl.get(i).getConfidence());
            if (positionArl.get(i).getFingerprint().getRoom_name() != null) {
               map.put(confidence, positionArl.get(i).getFingerprint()
                     .getRoom_name());
               max = Math.max(max, confidence);
            }
         } catch (Exception e) {
            Log.d(TAG, "skipping position", e);
         }
      }

      if (max <= 0)
         return null;

      result = new ArrayList<Position>();
      for (Map.Entry<Float, String> entry : map.entrySet()) {
         float key = entry.getKey() * 100 / max;
         String value = entry.getValue();
         Position pos = new Position();
         pos.setConfidence("" + key);
         Fingerprint fingerprint = new Fingerprint();
         fingerprint.setRoom_name(value);
         pos.setFingerprint(fingerprint);
         result.add(pos);
      }
      return result;
   }
}

