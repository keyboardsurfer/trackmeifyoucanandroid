package org.cbase.trackme.model;

/**
 * User: biafra
 * Date: 2/18/12
 * Time: 8:53 PM
 */
public class AnnounceSession {
  private String session;

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer();
    sb.append("AnnounceSession");
    sb.append("{session='").append(session).append('\'');
    sb.append('}');
    return sb.toString();
  }

  public String getSession() {
    return session;
  }

  public void setSession(String session) {
    this.session = session;
  }
}
