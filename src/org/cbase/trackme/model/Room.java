package org.cbase.trackme.model;

import java.util.List;

/**
 * User: biafra
 * Date: 2/18/12
 * Time: 3:36 PM
 */
public class Room {
  private String room;
  private List<String> users;

  public String getRoom() {
    return room;
  }

  public void setRoom(String room) {
    this.room = room;
  }

  public List<String> getUsers() {
    return users;
  }

  public void setUsers(List<String> users) {
    this.users = users;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer();
    sb.append("Room");
    sb.append("{name='").append(room).append('\'');
    sb.append(", users=").append(users);
    sb.append('}');
    return sb.toString();
  }
}
