package org.cbase.trackme.model;

public class Position {

   private String confidence;
   private Fingerprint fingerprint;

   public String getConfidence() {
      return confidence;
   }

   public void setConfidence(String confidence) {
      this.confidence = confidence;
   }

   public Fingerprint getFingerprint() {
      return fingerprint;
   }

   public void setFingerprint(Fingerprint fingerprint) {
      this.fingerprint = fingerprint;
   }

   @Override
   public String toString() {
      return "" + fingerprint.room_name +"(" + confidence +")";
   }
}
