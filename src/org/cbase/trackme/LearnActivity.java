package org.cbase.trackme;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.cbase.trackme.positioning.SaveRequest;

public class LearnActivity extends Activity implements OnClickListener {

  private static String TAG = LearnActivity.class.getSimpleName();

  public final static String KEY_ROOMNAME = "IKRN";

  private WifiManager  wifiManager;
  private WifiReceiver wifiReceiver;

  private List<ScanResult> scanResults;

  private Handler handler;

  private Button   learnAckBtn;
  private EditText learnET;
  private TextView learnProgressTV;

  private boolean save   = false;
  private boolean update = false;

  private String roomName = "not set";


  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putString(KEY_ROOMNAME, roomName);
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    roomName = savedInstanceState.getString(KEY_ROOMNAME);
    super.onRestoreInstanceState(savedInstanceState);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    handler = new Handler();
    wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
    setContentView(R.layout.learn_layout);


    Intent intent = getIntent();
    if (intent != null) {
      roomName = intent.getStringExtra(KEY_ROOMNAME);
    }

    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
    String macAddress = wifiInfo.getMacAddress();
    List<ScanResult> scanResults = wifiManager.getScanResults();
    wifiReceiver = new WifiReceiver();

    Log.d(TAG, "macAddress: " + macAddress);

    if (scanResults != null) {
      System.out.println(scanResults.size() + " wifi endpoints found:");
      for (ScanResult scanResult : scanResults) {
        Log.d(TAG, "- " + scanResult.BSSID + " " + scanResult.toString());
      }
    }

    learnET = (EditText) findViewById(R.id.learnET);
    learnET.setText(roomName);

    learnProgressTV = (TextView) findViewById(R.id.learnProgressTV);

    learnAckBtn = (Button) findViewById(R.id.learnACK);
    learnAckBtn.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        save = true;
      }
    });
  }

  @Override
  protected void onPause() {
    unregisterReceiver(wifiReceiver);
    super.onPause();
  }

  @Override
  protected void onResume() {
    registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

    Log.d(TAG, "onResume() -> startScan");
    wifiManager.startScan();
    super.onResume();
  }


  @Override
  public void onClick(View v) {
    switch (v.getId()) {

      case R.id.learnACK:
        Toast.makeText(LearnActivity.this, "Going to update ...", Toast.LENGTH_SHORT).show();
        //      update = true;
        //      learnProgressTV.setText("scanning...");
        //      wifiManager.startScan();
        //      save = true;
        updateMyLocation();
        break;
    }

  }


  // --------------------------------------------------------------

  private void updateMyLocation() {

    Log.d(TAG, "updateMyLocation()");

    new SaveRequest(new Date(), scanResults, learnET.getText().toString(), this) {

      @Override
      protected void finish() {
        Log.d(TAG, "location: " + learnET.getText());

        handler.post(new Runnable() {
          @Override
          public void run() {
            learnProgressTV.setText("done");
          }
        });

      }
    }.start();

    StringBuilder sb = new StringBuilder();
    for (ScanResult result : scanResults) {
      sb.append(result.BSSID).append(" ").append(result.level).append("\n");
    }
  }

  // =======================================================================

  class WifiReceiver extends BroadcastReceiver {
    private String TAG = WifiReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context c, Intent intent) {
      Log.i(TAG, "received wifi scans");

      scanResults = wifiManager.getScanResults();


      for (ScanResult scanResult : scanResults) {
        Log.d(TAG, "- " + scanResult.BSSID + " SSID: " + scanResult.SSID + " " + scanResult.level);
      }

      final String room = learnET.getText().toString();

      if (save) {
//           Intent resultIntent = new Intent();
//
//
//           resultIntent.;
//           LearnActivity.this.setResult(RESULT_OK, resultIntent);
//
        new SaveRequest(new Date(), scanResults, room, LearnActivity.this) {
          @Override
          protected void finish() {
            Log.d(TAG, "room: " + room);

            LearnActivity.this.finish();

          }

        }.start();


      }
      save = false;

      if (update) {
        updateMyLocation();
      }
      update = false;

      StringBuilder sb = new StringBuilder();
      for (ScanResult result : scanResults) {
        sb.append(result.BSSID).append(" ").append(result.level).append("\n");
      }
      //      scanResultsView.setText(sb.toString());

      //mainText.setText(sb);

      //      wifiManager.startScan();
    }
  }


}
