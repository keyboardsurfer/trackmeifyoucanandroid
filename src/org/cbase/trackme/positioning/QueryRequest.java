package org.cbase.trackme.positioning;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.List;

import org.cbase.trackme.PreferencesActivity;
import org.cbase.trackme.requests.Request;

import android.content.Context;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.util.Log;

public class QueryRequest extends Request {
   private static String TAG = QueryRequest.class.getSimpleName();

   
   private List<ScanResult> scanResults;

   public QueryRequest(final List<ScanResult> scanResults, Context context) {
      super(context);
      this.scanResults = scanResults;
   }

   @Override
   public void run() {

      try {
         Uri.Builder uri = Uri.parse(prefs.getString(PreferencesActivity.KEY_URL_LOCATIONSRVR, "no server") + "query_location").buildUpon();
         appendScanResults(uri, scanResults);
         Log.d(TAG, "" +uri.toString());

         final URLConnection connection = connect(uri.build());
         final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), ENCODING));

         String location = reader.readLine();
         Log.d("TAG", "location: " + location);
         // while (location != null) {
         //    result = result + location + "\n";
         //    location = reader.readLine();
         // }
         reader.close();

         finish(location);

      } catch (final Exception x) {
         Log.e(TAG, "", x);
      }
   }

   protected void finish(final String location) {
      // Overridden in anonymous subclass
   }

   @Override
   public String toString() {
      return "QueryRequest{" +
            "scanResults=" + scanResults +
            '}';
   }
}
