package org.cbase.trackme.positioning;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.text.TextUtils;
import android.util.Log;
import org.cbase.trackme.PreferencesActivity;
import org.cbase.trackme.requests.Request;

public class SaveRequest extends Request {

   private static String TAG = SaveRequest.class.getSimpleName();

   private Date             timestamp;
   private List<ScanResult> scanResults;
   private String           location;

   public SaveRequest(
         final Date timestamp, 
         final List<ScanResult> scanResults,
         final String location, 
         Context context)
   {
      super(context);
      this.timestamp = timestamp;
      this.scanResults = scanResults;
      if (!TextUtils.isEmpty(location)) {
         this.location = location.trim();
      } else {
         this.location = location;
      }
   }

   @Override
   public void run() {
      try {
         Uri.Builder uri = Uri.parse(prefs.getString(PreferencesActivity.KEY_URL_LOCATIONSRVR, "no server") + "save").buildUpon();
         uri.appendQueryParameter("timestamp", Long.toString(timestamp.getTime()));
         uri.appendQueryParameter("location", location);
         appendScanResults(uri, scanResults);

         final URLConnection connection = connect(uri.build());
         final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), ENCODING));
         String result = reader.readLine();
         Log.i(TAG, "result : " + result);
         reader.close();

         finish();
      } catch (final Exception x) {
         fail(x);
      }
   }

   protected void finish() {
   }

   @Override
   public String toString() {
      return "SaveRequest{" + "timestamp=" + timestamp + ", scanResults="
            + scanResults + ", location='" + location + '\'' + '}';
   }
}
