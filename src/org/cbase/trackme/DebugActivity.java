package org.cbase.trackme;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.cbase.trackme.adapter.RoomCompletionAdapter;
import org.cbase.trackme.announce.SaveRequest;
import org.cbase.trackme.announce.SessionRequest;
import org.cbase.trackme.model.AnnounceSession;
import org.cbase.trackme.positioning.QueryRequest;
import org.cbase.trackme.util.RoomAutocompleteTextView;

import com.google.gson.Gson;
import com.google.inject.Inject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

public class DebugActivity extends RoboActivity {
  private static String TAG = DebugActivity.class.getSimpleName();

  @Inject
  WifiManager wifiManager;

  @InjectView(R.id.update_button)
  Button                   updateButton;
  @InjectView(R.id.save_button)
  Button                   saveButton;
  @InjectView(R.id.room_auto_complete)
  RoomAutocompleteTextView roomView;
  @InjectView(R.id.scan_results)
  TextView                 scanResultsView;
  @InjectView(R.id.location)
  TextView                 locationView;

  WifiReceiver receiverWifi;

  private GsmCellLocation gsmCellLocation;
  private int             cellStrength;
  private int             mcc;
  private int             mnc;


  private Handler          handler;
  private List<ScanResult> scanResults;

  private boolean save   = false;
  private boolean update = false;

  private SharedPreferences prefs;

  @Override
  public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.debug);

    prefs = PreferenceManager.getDefaultSharedPreferences(this);
    handler = new Handler();

    final RoomAutocompleteTextView textView = (RoomAutocompleteTextView) findViewById(R.id.room_auto_complete);
    RoomCompletionAdapter adapter = new RoomCompletionAdapter(this,
                                                              R.layout.list_item_room_name,
                                                              new ArrayList<String>());
    textView.setAdapter(adapter);
    textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "foooo");
      }
    });

    WifiInfo wi = wifiManager.getConnectionInfo();
    String ma = wi.getMacAddress();

    Log.d(TAG, "ma: " + ma);

    List<ScanResult> scanResults = wifiManager.getScanResults();
    if (scanResults != null) {
      System.out.println(scanResults.size() + " wifi endpoints found:");
      for (ScanResult scanResult : scanResults) {
        Log.d(TAG, "- " + scanResult.BSSID + " " + scanResult.toString());
      }
    }

    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
    String networkOperator = tm.getNetworkOperator();
    mcc = -1;
    mnc = -1;
    if (networkOperator != null) {
      mcc = Integer.parseInt(networkOperator.substring(0, 3));
      mnc = Integer.parseInt(networkOperator.substring(3));
    }

    gsmCellLocation = (GsmCellLocation) tm.getCellLocation();
    Log.d(TAG, "CellLocation: " + " mcc: " + mcc + " mnc: " + mnc + " cid: " + gsmCellLocation.getCid()
               + " lac: " + gsmCellLocation.getLac() + " psc: " + gsmCellLocation.getPsc()
         );
    int flags = PhoneStateListener.LISTEN_SIGNAL_STRENGTHS | PhoneStateListener.LISTEN_CELL_LOCATION;

    tm.listen(new PhoneStateListener() {
      @Override
      public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        Log.d(TAG, "PhoneStateListener: onSignalStrengthsChanged: signalStrength.getGsmSignalStrength(): " + signalStrength.getGsmSignalStrength());
        cellStrength = signalStrength.getGsmSignalStrength();

        updateMyLocation();
      }

      @Override
      public void onCellLocationChanged(CellLocation cell) {
        gsmCellLocation = (GsmCellLocation) cell;
        Log.d(TAG, "PhoneStateListener: CellLocation: cid: " + gsmCellLocation.getCid()
                   + " lac: " + gsmCellLocation.getLac()
             );
        updateMyLocation();
      }
    }, flags);
    List<NeighboringCellInfo> neighbours = tm.getNeighboringCellInfo();
    Log.d(TAG, "Found " + neighbours.size() + " neighbours");
    for (NeighboringCellInfo neighboringCellInfo : neighbours) {
      Log.d(TAG, "Neighbour: cid: " + neighboringCellInfo.getCid() + " lac: " + neighboringCellInfo.getLac() + " rssi: " + neighboringCellInfo.getRssi());
    }


    receiverWifi = new WifiReceiver();

    updateButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Toast.makeText(DebugActivity.this, "Update ...", Toast.LENGTH_SHORT).show();
        update = true;
        locationView.setText("scanning...");
        wifiManager.startScan();

      }
    });

    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        Toast.makeText(DebugActivity.this, "Save ...", Toast.LENGTH_SHORT).show();

        save = true;
        wifiManager.startScan();

      }
    });

  }

  @Override
  protected void onPause() {
    unregisterReceiver(receiverWifi);
    super.onPause();
  }

  @Override
  protected void onResume() {
    registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

    Log.d(TAG, "startScan");
    wifiManager.startScan();
    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
    String networkOperator = tm.getNetworkOperator();
    int mcc = -1;
    int mnc = -1;
    if (networkOperator != null) {
      mcc = Integer.parseInt(networkOperator.substring(0, 3));
      mnc = Integer.parseInt(networkOperator.substring(3));
    }

    GsmCellLocation cellLocation = (GsmCellLocation) tm.getCellLocation();
    Log.d(TAG, "CellLocation: " + " mcc: " + mcc + " mnc: " + mnc + " cid: " + cellLocation.getCid()
               + " lac: " + cellLocation.getLac()
         );
    super.onResume();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.debug_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.central_menu_prefs:
        Intent prefsIntent = new Intent(this, PreferencesActivity.class);
        this.startActivityForResult(prefsIntent, 0);
        break;

      case R.id.central_menu_search: {
        Intent searchIntent = new Intent(this, SearchActivity.class);
        this.startActivityForResult(searchIntent, 0);
        break;
      }

      case R.id.central_menu_room_list: {
        Intent roomList = new Intent(this, RoomListActivity.class);
        this.startActivityForResult(roomList, 0);
        break;
      }

      case R.id.central_menu_central: {
        Intent roomList = new Intent(this, CentralActivity.class);
        this.startActivityForResult(roomList, 0);
        break;
      }
    }
    return false;
  }

  private void updateMyLocation() {

    new QueryRequest(scanResults, this) {

      @Override
      protected void finish(final String location) {
        Log.d(TAG, "=========================== HAT GEKLAPPT!!!: " + this);
        Log.d(TAG, "===========================        location: " + location);

        handler.post(new Runnable() {
          @Override
          public void run() {

            locationView.setText(location);
          }
        });

      }
    }.start();

    String scanResultsString = createScanResultsString();
    scanResultsView.setText(scanResultsString);
  }

  private String createScanResultsString() {
    StringBuilder sb = new StringBuilder();
    if (scanResults != null) {
      for (ScanResult result : scanResults) {
        sb.append(result.BSSID)
          .append(" ")
          .append(result.level)
          .append(" [")
          .append(result.SSID)
          .append("] ")
          .append(result.frequency)
//        .append(" (cap: ")
//        .append(result.capabilities)
//        .append(") ")
          .append("\n");
      }
    }
    sb.append("\n")
      .append(mcc)
      .append(":")
      .append(mnc)
      .append(":")
      .append(gsmCellLocation.getLac())
      .append(":")
      .append(gsmCellLocation.getCid())
      .append(" ")
      .append(cellStrength);

    return sb.toString();
  }

  class WifiReceiver extends BroadcastReceiver {
    private String TAG = WifiReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context c, Intent intent) {
      Log.i(TAG, "Received wifi scans");

      scanResults = wifiManager.getScanResults();
      //      for (ScanResult scanResult : scanResults) {
      //        Log.d(TAG, "- " + scanResult.BSSID + " SSID: " + scanResult.SSID + " " + scanResult.level);
      //      }
      final String room = roomView.getText().toString();

      if (save) {

        new org.cbase.trackme.positioning.SaveRequest(new Date(), scanResults, room, DebugActivity.this) {
          @Override
          protected void finish() {
            Log.d(TAG, "SaveRequest.finish ");
            handler.post(new Runnable() {
              @Override
              public void run() {
                Toast.makeText(DebugActivity.this, "...Saved position", Toast.LENGTH_SHORT).show();
              }
            });

          }

        }.start();

        //1. get session here if we have none yet
        // TODO: use constant for username key here
        new SessionRequest(prefs.getString(PreferencesActivity.KEY_URL_USERNAME, "no username"),
                           DebugActivity.this) {

          @Override
          protected void finish(final String result) {
            Log.d(TAG, "AnnounceSessionRequest.result: " + result);

            if (!result.equals("failed to get location")) {

              Gson gson = new Gson();
              final AnnounceSession session = gson.fromJson(result, AnnounceSession.class);
              handler.post(new Runnable() {
                @Override
                public void run() {

                  Toast.makeText(DebugActivity.this,
                                 "AnnounceSessionRequest: " + session.getSession(),
                                 Toast.LENGTH_SHORT).show();
                }
              });
              //2. save room to announcement server
              new SaveRequest(session.getSession(), room, DebugActivity.this) {
                protected void finish(final String result) {

                  Log.d(TAG, "AnnounceSaveRequest.result: " + result);
                  handler.post(new Runnable() {
                    @Override
                    public void run() {

                      Toast.makeText(DebugActivity.this, "AnnounceSaveRequest: " + result, Toast.LENGTH_SHORT)
                           .show();
                    }
                  });

                }

              }.start();

              handler.post(new Runnable() {
                @Override
                public void run() {

                  locationView.setText(result);
                }
              });
            }
          }
        }.start();

      }
      save = false;

      if (update) {
        updateMyLocation();
      }
      update = false;

      String scanResultsString = createScanResultsString();
      scanResultsView.setText(scanResultsString);

    }
  }

}
